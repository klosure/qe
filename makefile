PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

deps:
	cp sample-qerc ~/.qerc
help:
	@echo "please run 'make install' as root"
install:
	cp ./qe.sh $(BINDIR)/qe
	cp ./qe.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/qe
	rm $(MANDIR)/qe.1
	rm ~/.qerc
test:
	./tests/runner.sh
