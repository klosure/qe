# qe

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

### About
This command line tool allows us to **Q**uickly **E**ncrypt a file or folder. The goal is to have a minimal interface where we do not have to memorize a bunch of flags. qe uses *AES256* symmetric cipher. GPG's default key stretching in symmetric mode is [relatively weak](https://crypto.stackexchange.com/questions/48906/what-exactly-does-s2k-do-in-gpg). To alleviate this issue we derive our key from argon2 first, then pass it to GPG. I have made an effort to keep this tool as portable as possible. Should work with any POSIX shell. While compression is supported, `qe` does not compress anything by default.

### Dependencies
+ [GPG](https://www.gnupg.org/)
+ [argon2](https://github.com/P-H-C/phc-winner-argon2)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)
+ [zstd](https://github.com/facebook/zstd) (*optional*)

### Installation
Simply run:

```bash
git clone https://gitlab.com/klosure/qe.git
cd qe
make deps

# as root
make install

# to uninstall
make uninstall

# to run development unit tests (pulls down shunit2)
git submodule init
git submodule update
```

> Note: When you pass a directory, qe will tar it prior to encryption!

### Usage
```bash
qe secret.txt                    # encrypt secret.txt
qe /root/secret\ dir/            # encrypt the directory 'secret dir'
qe secret.txt.gpg                # decrypt secret.txt.gpg
qe -z gz secret.txt              # gzip secret.txt prior to encryption
qe -z zst /root/secret\ dir/     # compress using zstd prior to encryption
qe -h                            # show this help message
qe -v                            # show version

# qe expects encrypted files to end with the .gpg extension
```

### Configuration
If you would like to set the default behavior of `qe`, it looks for a configuration file in your home directory `~/.qerc`. This will help you by allowing you to skip entering flags manually.

### Technical Notes
[Read More.](NOTES.md)

### License / Disclaimer
This project is licensed under the 3-clause BSD license. (See LICENSE.md)

I take no responsibility for you blowing stuff up.
