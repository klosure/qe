# Technical Notes

Originally this tool was written to use OpenSSL. After spending some time reading up on OpenSSL's enc CLI it was apparent that there are too many gotchas to continue using it. Some users online recommend it, like [here](https://jameshfisher.com/2017/03/09/openssl-enc/) and [here](https://www.shellhacks.com/encrypt-decrypt-file-password-openssl/), but they don't address the known problems. In short:

+ Uses a weak key derivation function by default: MD5 or SHA, without key stretching.
+ Using CBC mode from the enc interface does not support authentication.
+ The enc interface does not support AES-GCM authentication mode
+ Using the same password to encrypt multiple files will result in IV re-use (bad!).

While newer versions of OpenSSL include changes to address some of the issues, if the recipient isn't up to date on their software they're out of luck. Moving this tool to use GPG addresses all the above issues. Since the goal of the qe tool is to use a strong and sane defaults and provide a dead simple interface, using GPG is simply easier than working around the holes in OpenSSL. If you want to read more about some of the current issues facing OpenSSL, I'll include some links below.

Readings:
[1](https://security.stackexchange.com/questions/182277/is-openssl-aes-256-cbc-encryption-safe-for-offsite-backup), [2](https://www.exploit-db.com/papers/41019)
