#!/bin/sh
#
# author: klosure
# https://gitlab.com/klosure/qe
# license: bsd 3-clause
#
# --- "Quickly Encrypt" using GPG ---
# I can't ever remeber all the proper flags. With this I don't need to.
# Encrypted files will have the .gpg extension appended to the filename.
# Also, has the handy feature of automatically handling compression. (if desired)
# 
# Default cipher: AES256
# During decryption, files are expected to end with the .gpg extension
#
# Configuration: see sample-qerc


VERSION=0.3.5

# -- GLOBAL VARS --
IN_FILE="file.txt" # the file targeted to be processed
OUT_FILE="file.txt.gpg" # the resulting file after processing is complete
CIPHER="AES256" # cipher for gpg to use
PASS_PIPE="/tmp/qe_pass_pipe" # we use this pipe to transport the user provided password so that we don't have to pass it as an argument and expose it to the process list.
GEN_EARGS="--quiet --no-verbose --batch --passphrase-file $PASS_PIPE" # gpg settings for encryption
GEN_DARGS="--quiet --no-verbose --batch --passphrase-fd 0" # gpg settings for decryption
E_ARGS="--symmetric --no-symkey-cache --s2k-mode=1 --cipher-algo $CIPHER" # used to tell gpg we are encrypting a file
D_ARGS="--decrypt" # used to tell gpg we are decrypting a file
SALT="LetsAddALittleSaltIntoTheMix" # salt value to use with argon2
ARGON_PARAMS="-r -d -t 3 -m 16 -p 2 -l 128" # flags to be used by argon2
PASS1="1" # password will be read in from the user to here
PASS2="2" # the second password must match the first
GPG_COMMAND="" # gpg command to be applied to target file
TAR_COMMAND="" # tar command to be applied to target file
COMP_COMMAND="" # compression command to be applied to target file
WORKING_MESSAGE="" # message to be printed out to the user to let them know work is being done

# default compression settings (can be overriden via ~/.qerc)
COMPRESS_ALGO=""   # gz or zst 
COMPRESS_GZ_ARGS="-c -q" # settings when using gzip
COMPRESS_ZST_ARGS="-c -z -q --adapt=min=4,max=18" # settings when using zstd


# clean up old named pipe
fun_check_pass_pipe () {
    if [ -p "$PASS_PIPE" ] ; then
	rm "$PASS_PIPE"
    fi
}


# wipe out global password variables and unset them
fun_clear_pass_vars () {
    PASS1=""
    PASS2=""
    unset PASS1 PASS2
}


# called on ctrl-c press
fun_cleanup () {
    echo "qe: caught sigint, cleaning up..."
    fun_check_pass_pipe
    fun_clear_pass_vars
    exit 0
}


# this function is used to work on directories only
# if the target is an individual file, fun_compress () is used
fun_compress_dir () {
    IN_FILE="$*"
    BASE_DIR=$(basename "$IN_FILE")
    WORKING_MESSAGE="Compressing tarball of: $IN_FILE"
    if [ "$COMPRESS_ALGO" = "gz" ] ; then
	OUT_FILE="$BASE_DIR".tar.gz
	TAR_COMMAND="tar cf - "
	COMP_COMMAND="gzip $COMPRESS_GZ_ARGS"
    elif [ "$COMPRESS_ALGO" = "zst" ] ; then
	OUT_FILE="$BASE_DIR".tar.zst
	TAR_COMMAND="tar cf - "
	COMP_COMMAND="zstd $COMPRESS_ZST_ARGS"
    else
	unset BASE_DIR
	echo "bad state! compression value corrupted? debug in fun_compress_dir()"
	exit 2
    fi
    unset BASE_DIR
}


# our target is a directory, so we need to archive it.
fun_tar_dir () {
    if ! [ "$COMPRESS_ALGO" = "" ] ; then
	# tar AND compress
	fun_compress_dir "$*"
    else
	# only tar, no compression
	IN_FILE="$*"
	WORKING_MESSAGE="Creating tarball..."
	BASE_DIR=$(basename "$IN_FILE")
	OUT_FILE="$BASE_DIR".tar
	TAR_COMMAND="tar cf - "
	unset BASE_DIR
    fi
}


# this function is used to work on files only
# if the target is a directory, fun_compress_dir () is used 
fun_compress () {
    IN_FILE="$*"
    BASE_FN=$(basename "$IN_FILE")
    OUT_FILE="$BASE_FN"
    if ! [ "$COMPRESS_ALGO" = "" ] ; then
	WORKING_MESSAGE="Compressing file: $IN_FILE"
	if [ "$COMPRESS_ALGO" = "gz" ] ; then
	    OUT_FILE="$OUT_FILE".gz
	    COMP_COMMAND="gzip $COMPRESS_GZ_ARGS"
	elif [ "$COMPRESS_ALGO" = "zst" ] ; then
	    OUT_FILE="$OUT_FILE".zst
	    COMP_COMMAND="zstd $COMPRESS_ZST_ARGS"
	else
	    unset BASE_FN
	    echo "bad state! compression value corrupted? debug in fun_compress()"
	    exit 2
	fi
    fi
    unset BASE_FN
}


# is the filename that was passed in actually a directory?
fun_is_dir () {
    if [ -d "$*" ] ; then
	fun_tar_dir "$*"
	return 0 # true
    else
	if [ -s "$*" ] ; then
	    IN_FILE="$*"
	    return 1 # false
	else
	    # bad filename was passed in
	    fun_error "Bad filename given!"
	fi
    fi
}


# sets the output filename in decryption
fun_set_file_dec () {
    IN_FILE="$*"
    if [ -f "$IN_FILE" ] && [ -s "$IN_FILE" ] ; then
	WORKING_MESSAGE="Decrypting file: $IN_FILE"
	# strip off the .gpg
	OUT_FILE="${IN_FILE%.*}"
    else
	# Were we given an empty file to decrypt?
	# Were we given a directory ending in .gpg ? Why???
	# regardless, a bad state.
	fun_error "Given an empty file, or directy containing '.gpg'!"
    fi
}


# sets the output filename in encryption
fun_set_file_enc () {
    if ! fun_is_dir "$*" ; then
	fun_compress "$*"
    fi
    EXT="gpg"
    OUT_FILE=$OUT_FILE"."$EXT
}


# prompt the user for a password 
fun_get_pass () {
    B_IFS=$IFS && IFS=""
    stty -echo
    trap 'stty echo' EXIT
    printf "Password: "
    read -r PASS1
    if [ "$1" = "enc" ] ; then
	printf "\n"
	printf "Repeat Password: "
	read -r PASS2
    fi
    stty echo
    trap - EXIT
    printf "\n"
    IFS=${B_IFS}
}


# main encryption function
# most of processing happens here
fun_encrypt () {    
    fun_set_file_enc "$*"
    fun_get_pass "enc"
    if [ "$PASS1" = "$PASS2" ] ; then
	echo "$WORKING_MESSAGE"
	# shellcheck disable=SC2086
	ARGON_KEY=$(echo "$PASS1" | argon2 "$SALT" $ARGON_PARAMS)
	mkfifo "$PASS_PIPE" # create the pipe to transport the password
	echo "$ARGON_KEY" > "$PASS_PIPE" & # send the password down the pipe
	if ! [ "$TAR_COMMAND" = "" ] && ! [ "$COMP_COMMAND" = "" ] ; then
	    # we are creating a dir tarball, compressing it, then encrypting it.
	    # shellcheck disable=SC2086
	    $TAR_COMMAND "$IN_FILE" | $COMP_COMMAND | $GPG_COMMAND $GEN_EARGS --output "$OUT_FILE" $E_ARGS
	elif ! [ "$TAR_COMMAND" = "" ]  ; then
	    # we are creating a dir tarball, then encrypting it.
	    # shellcheck disable=SC2086
	    $TAR_COMMAND "$IN_FILE" | $GPG_COMMAND $GEN_EARGS --output "$OUT_FILE" $E_ARGS
	elif ! [ "$COMP_COMMAND" = "" ] ; then
	    # we are compressing the file, then encrypting it.
	    # shellcheck disable=SC2086
	    $COMP_COMMAND "$IN_FILE" | $GPG_COMMAND $GEN_EARGS --output "$OUT_FILE" $E_ARGS
	else
	    # no extra processing, we are only encrypting the file.
	    # shellcheck disable=SC2086
	    $GPG_COMMAND $GEN_EARGS --output "$OUT_FILE" $E_ARGS "$IN_FILE"
	fi     
	rm "$PASS_PIPE"
	ARGON_KEY=""
	unset ARGON_KEY
    else
	echo "Passwords did not match."
    fi
}


# main decryption function
fun_decrypt () {
    fun_set_file_dec "$*"
    fun_get_pass "dec"
    echo "$WORKING_MESSAGE"
    # shellcheck disable=SC2086
    echo "$PASS1" | \
	argon2 $SALT $ARGON_PARAMS | \
	$GPG_COMMAND $GEN_DARGS --output "$OUT_FILE" $D_ARGS "$IN_FILE"
}


# called when an error occurred
# $1 is the message to be printed
# then cleanup and exit out!
fun_error () {
    echo "$1"
    echo ""
    fun_print_usage
    fun_clear_pass_vars
    exit 1
}


# checks if we are encrypting or decrypting a file
fun_check_mode () {
    case "$@" in
	*.gpg)  fun_decrypt "$*" ;;
        *)	fun_encrypt "$*" ;;
    esac
}


# print out the help info on how to run the program
fun_print_usage () {
    echo "usage: qe secret.txt                    # encrypt secret.txt"
    echo "       qe secret.txt.gpg                # decrypt secret.txt.gpg [*]"
    echo "       qe /root/secret\ dir/            # encrypt the directory 'secret dir' [**]"
    echo "       qe -z gz secret.txt              # gzip secret.txt prior to encryption"
    echo "       qe -z zst /root/secret\ dir/     # compress using zstd prior to encryption"
    echo "       qe -h                            # show this help message"
    echo "       qe -v                            # show version"
    echo ""
    echo "*  (expects encrypted file to end with the .gpg extension)"
    echo "** (passing qe a directory will tar it prior to encryption)"
}


# will be called when run with the -v flag
fun_print_ver () {
    echo "qe version: $VERSION"
}


# show the user our program, license, and author info
fun_print_info () {
    echo "qe - quick encryption with GPG"
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/qe"
    echo "license: bsd 3-clause license"
    fun_print_ver
    echo ""
    fun_print_usage
}


# The -z flag was passed in
# make sure that we were given a valid option
fun_check_compress () {
    case "$2" in
	gz) COMPRESS_ALGO="gz" ;;
        zst) COMPRESS_ALGO="zst" ;;
	*) fun_error "That was not a recognized compression option" ;;
    esac
    shift 2
    fun_check_mode "$@"
}


# parse what arguments were passed in
# call a corresponding function
fun_check_args () {
    if [ "$#" -gt 0 ] ; then
	case "$1" in
            -h)	 fun_print_info "$@" ;;
            -v)  fun_print_ver "$@" ;;
	    -z)  fun_check_compress "$@" ;;
            *)	 fun_check_mode "$@" ;;
	esac
    else
	fun_error "Pass in a filename or flag!"
    fi
}


# look for rc (i.e. config) file
# (see the included sample-qerc for an example!)
fun_check_rc () {
    if [ -s ~/.qerc ] ; then
	# shellcheck source=/dev/null
	. ~/.qerc # the values in here will override the above compression vars
    fi
}


# make sure that our dependencies are installed on the system
fun_check_deps () {
    if [ -x "$(command -v gpg)" ]; then
	GPG_COMMAND="gpg"
    elif [ -x "$(command -v gpg2)" ]; then
	GPG_COMMAND="gpg2"
    else
	fun_error "error: qe needs to have gpg installed"
    fi
    if ! [ -x "$(command -v argon2)" ]; then
	fun_error "error: qe needs argon2, please install it."
    fi
}


# --- ENTRY POINT ---
trap 'fun_cleanup' INT # catch a ctrl-c
fun_check_pass_pipe
fun_check_deps
fun_check_rc
fun_check_args "$@"
fun_clear_pass_vars
exit 0 # all is well!
